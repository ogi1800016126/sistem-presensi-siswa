import { LupaSandiPage } from './../lupa-sandi/lupa-sandi.page';
import { RegisterPage } from './../register/register.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginPage } from './login.page';
import { UserPage } from '../user/user.page';

const routes: Routes = [
  {
    path: '',
    component: LoginPage
  },
  {
    path: '',
    component: UserPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../home/home.module').then(m => m.HomePageModule)
      },
      {
        path: 'history',
        loadChildren: () => import('../rekap/rekap.module').then(m => m.RekapPageModule)
      },
      {
        path: 'voucher',
        loadChildren: () => import('../jadwal/jadwal.module').then(m => m.JadwalPageModule)
      },
      {
        path: 'profile',
        loadChildren: () => import('../profile/profile.module').then(m => m.ProfilePageModule)
      },
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'register',
    component: RegisterPage
  },
  {
    path: 'lupasandi',
    component: LupaSandiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginPageRoutingModule { }
