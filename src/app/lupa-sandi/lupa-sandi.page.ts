import { LoadingController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Component, OnInit } from '@angular/core';
import firebase from 'firebase/app';


interface User {
    email: string;
}

@Component({
    selector: 'app-lupa-sandi',
    templateUrl: './lupa-sandi.page.html',
    styleUrls: ['./lupa-sandi.page.scss'],
})
export class LupaSandiPage implements OnInit {


    user: any = {};
    userData: User;
    loading: boolean;
    constructor(
        public auth: AngularFireAuth,
        public toast: ToastController,
        public router: Router) { }

    ngOnInit() {
    }

    async pesan(message, color) {
        const toast = await this.toast.create({
            message,
            duration: 3000,
            color,
            position: 'top'
        });
        toast.present();
    }

    async resetPassword(email) {

        if (this.user.email) {
            this.loading = true;
            this.auth.sendPasswordResetEmail(this.user.email).then(() => {
                this.pesan('Silahkan Buka Email Untuk Reset Password', 'success');
                this.router.navigate(['/login']);
            }).catch(err => {
                this.pesan('Maaf email anda tidak terdaftar', 'danger');
                this.loading = false;
            });
        }
        else {
            this.pesan('Masukan Email Terlebih Dahulu', 'danger');
        }
    }


}
