import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LupaSandiPage } from './lupa-sandi.page';

describe('LupaSandiPage', () => {
  let component: LupaSandiPage;
  let fixture: ComponentFixture<LupaSandiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LupaSandiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LupaSandiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
