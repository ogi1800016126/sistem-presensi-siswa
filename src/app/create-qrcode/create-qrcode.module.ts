import { NgxQRCodeModule } from 'ngx-qrcode2';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateQrcodePageRoutingModule } from './create-qrcode-routing.module';

import { CreateQrcodePage } from './create-qrcode.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateQrcodePageRoutingModule,
    NgxQRCodeModule,
  ],
  declarations: [CreateQrcodePage],
})
export class CreateQrcodePageModule { }
