import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateQrcodePage } from './create-qrcode.page';

describe('CreateQrcodePage', () => {
  let component: CreateQrcodePage;
  let fixture: ComponentFixture<CreateQrcodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateQrcodePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateQrcodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
