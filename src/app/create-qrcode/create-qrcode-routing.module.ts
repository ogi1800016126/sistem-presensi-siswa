import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateQrcodePage } from './create-qrcode.page';

const routes: Routes = [
  {
    path: '',
    component: CreateQrcodePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateQrcodePageRoutingModule {}
