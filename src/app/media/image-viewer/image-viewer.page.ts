import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-image-viewer',
  templateUrl: './image-viewer.page.html',
  styleUrls: ['./image-viewer.page.scss'],
})
export class ImageViewerPage implements OnInit {
  @Input() imageData:any;
  @Input() title: any;
  constructor(
    public modalCtrl: ModalController
  ) { }

  ngOnInit() {
    if(this.title == undefined) this.title = 'Image Viewer';
  }

  dismiss()
  {
    this.modalCtrl.dismiss();
  }

}
