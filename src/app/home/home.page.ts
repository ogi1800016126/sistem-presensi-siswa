import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
 
 data : any = {};
 userData: any ={};

  constructor( 
private db: AngularFirestore,
private auth: AngularFireAuth,
public router: Router
) {}

  ngOnInit() {
  this.auth.onAuthStateChanged(user=>{
  this.userData = user;
 })
 }
 tambah() {
    this.router.navigate(['/create-qrcode']);
  }

  loading:boolean;
  simpanKegiatan()
  {
  this.loading = true ;
  //menambahkan atribut author
  this.data.author = this.userData.email;
  var doc = new Date().getTime(). toString();
  this.db.collection('events').doc(doc).set(this.data).then(res=>{
  this.loading = false;
  alert('Data Absen Berhasil Disimpan');
  }).catch(err=>{
  this.loading = false;
  alert('Tidak Dapat Menyimpan Data Absen')
  })

}
}
