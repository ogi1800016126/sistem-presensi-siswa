import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ScanningPageRoutingModule } from './scanning-routing.module';

import { ScanningPage } from './scanning.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ScanningPageRoutingModule
  ],
  declarations: [ScanningPage]
})
export class ScanningPageModule {}
