import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ScanningPage } from './scanning.page';

describe('ScanningPage', () => {
  let component: ScanningPage;
  let fixture: ComponentFixture<ScanningPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScanningPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ScanningPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
