import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';
import { ToastController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';

interface User {
    nama: string;
    email: string;
}

@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

    user: any = {};
    userData: User;

    constructor(public route: Router,
        public auth: AngularFireAuth,
        public db: AngularFirestore,
        public toastController: ToastController,
        public router: Router) { }
    loading: boolean;

    ngOnInit() {
    }

    async pesan(message, color) {
        const toast = await this.toastController.create({
            message,
            duration: 1000,
            color,
            position: 'top'
        });
        toast.present();
    }

    daftar() {
        this.auth.createUserWithEmailAndPassword(this.user.email, this.user.password).then(rest => {
            this.createUser(rest.user.email);
        }, err => {
            this.loading = false;
            this.pesan('Email Sudah Pernah Digunakan', 'danger');
        }
        );
    }

    createUser(email) {
        this.userData = {
            nama: this.user.fullname,
            email: this.user.email
        };
        this.loading = true;
        this.db.collection('users').doc(email).set(this.userData).then(res => {
            this.pesan('Selamat, Registrasi Akun Sukses', 'success');
            this.router.navigate(['/login']);
        }, err => {
            this.loading = false;
            this.pesan('Registrasi Akun Gagal', 'danger');
        });
    }

}
